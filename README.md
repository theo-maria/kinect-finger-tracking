# Kinect Finger Tracking

The goal of this project is to experiment on using a depth camera in order to
detect the movement of fingers on a surface.

We are currently working on several approaches with OpenCV in C#, by using
scientific papers to implement our algorithms.