﻿using OpenCvSharp;
using KinectModule;

namespace ImageProcessing
{
    class DepthImageThresholder
    {
        private static string TARGET_WINDOW_NAME = "Threshold";
        private static string THRESHOLD_TRACKBAR_NAME = "Threshold";
        private static int MAX_THRESHOLD_VALUE = 8000;
        private static int DEFAULT_THRESHOLD_VALUE = 1000;

        private static int thresholdValue;
        private Mat outputFrame;



        public DepthImageThresholder()
        {
            outputFrame = new Mat(KinectDepthImageGetter.H, KinectDepthImageGetter.W, MatType.CV_16UC1);
            thresholdValue = DEFAULT_THRESHOLD_VALUE;
            AddTrackbar();
        }

        public Mat GetOutputFrame(Mat inputFrame)
        {
            Cv2.Threshold(inputFrame, outputFrame, thresholdValue, MAX_THRESHOLD_VALUE, ThresholdTypes.TozeroInv);
            return outputFrame;
        }

        private static void AddTrackbar()
        {
            CvTrackbarCallback callback = new CvTrackbarCallback(TrackBarValueChanged);
            CvTrackbar thresholdTrackBar = new CvTrackbar(THRESHOLD_TRACKBAR_NAME, TARGET_WINDOW_NAME, DEFAULT_THRESHOLD_VALUE, MAX_THRESHOLD_VALUE, callback);
        }

        private static void TrackBarValueChanged(int pos)
        {
            thresholdValue = pos;
        }
    }
}
