﻿
using OpenCvSharp;
using System;
using System.Collections.Generic;

namespace ImageProcessing
{
    public class ClickDetection
    {
        ushort detectionSize = 15;

        public bool FindClick(Mat depthDatas, List<int> fingersPositions)
        {
            if (fingersPositions.Count < 6)
            {
                return false;
            }
            for (int i = 0; i < fingersPositions.Count; i += 6)
            {
                int yStart = fingersPositions[i + 1];
                int yEnd = fingersPositions[i + 2];
                int xStart = fingersPositions[i];
                int yStart2 = fingersPositions[i + 4];
                int yEnd2 = fingersPositions[i + 5];
                int xEnd = fingersPositions[i + 3];
                ushort startValue = depthDatas.Get<ushort>(yStart, xStart);
                ushort endValue = depthDatas.Get<ushort>(yEnd, xStart);
                ushort startValue2 = depthDatas.Get<ushort>(yStart2, xEnd);
                ushort endValue2 = depthDatas.Get<ushort>(yEnd2, xEnd);

                if (startValue - depthDatas.Get<ushort>(yStart - 3, xStart) < detectionSize &&
                    endValue - depthDatas.Get<ushort>(yEnd + 3, xStart) < detectionSize ||
                    startValue2 - depthDatas.Get<ushort>(yStart2 - 3, xEnd) < detectionSize &&
                    endValue2 - depthDatas.Get<ushort>(yEnd2 + 3, xEnd) < detectionSize)
                {
                    return true;
                }
            }
            return false;
        }

    }

}