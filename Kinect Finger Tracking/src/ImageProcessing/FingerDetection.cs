﻿using OpenCvSharp;
using KinectModule;
using System;
using System.Collections.Generic;

namespace ImageProcessing
{
    class FingerDetection
    {
        int sizeX = KinectDepthImageGetter.W;
        int sizeY = KinectDepthImageGetter.H;
        double atan60 = Math.Atan(60);
        double minHeightFinger = 5, maxHeightFinger = 25;
        List<int> outData = new List<int>(), finalData = new List<int>();

        //private Mat outputFrame;

        public FingerDetection()
        {
            //outputFrame = new Mat(sizeY, sizeX, MatType.CV_16UC1);
        }

        public List<int> FindCylinder(Mat inputFrame, Mat inputDistance)
        {
            //outputFrame = Mat.Zeros(sizeY, sizeX, MatType.CV_16UC1);
            outData.Clear();
            finalData.Clear();
            bool isFinger = true;
            int countInCylinder = 0;
            for ( int i = 0; i < sizeX ; ++i)
            {
                for (int j = 0 ; j < sizeY - 1; ++j)
                {
                    if (inputFrame.Get<short>(j,i) > 5000)
                    {
                        double heightMax = atan60 * inputDistance.Get<ushort>(j, i);
                        double pixelSize = sizeY / heightMax;
                        ushort maxPixelSize = (ushort) (pixelSize * maxHeightFinger);
                        do
                        {
                            ++countInCylinder;
                            if (countInCylinder >= maxPixelSize)
                            {
                                isFinger = false;
                                break;
                            }
                        } while (inputFrame.Get<short>(j + countInCylinder, i) >= -500);
                        if (isFinger && countInCylinder > pixelSize * minHeightFinger)
                        {
                            outData.Add(i);
                            outData.Add(j);
                            outData.Add(j + countInCylinder);

                            //for (int max = j + countInCylinder ; j < max ; ++j)
                            //{
                            //    outputFrame.Set(j, i, 1500);
                            //}
                        }
                        countInCylinder = 0;
                        isFinger = true;
                    }
                }
            }

            if (outData.Count > 0)
            {
                int count = 0;
                finalData.Add(outData[0]);
                finalData.Add(outData[1]);
                finalData.Add(outData[2]);
                for (int i = 3; i < outData.Count; i += 3)
                {
                    if (outData[i] == outData[i - 3]) continue;
                    if (outData[i] != outData[i - 3] + 1)
                    {
                        if (count >= 5)
                        {
                            finalData.Add(outData[i - 3]);
                            finalData.Add(outData[i - 2]);
                            finalData.Add(outData[i - 1]);
                        }
                        else
                        {
                            finalData.RemoveAt(finalData.Count - 1);
                            finalData.RemoveAt(finalData.Count - 1);
                            finalData.RemoveAt(finalData.Count - 1);
                        }
                        finalData.Add(outData[i]);
                        finalData.Add(outData[i + 1]);
                        finalData.Add(outData[i + 2]);
                        count = 0;
                    }

                    ++count;
                }

                if (count < 5)
                {
                    finalData.RemoveAt(finalData.Count - 1);
                    finalData.RemoveAt(finalData.Count - 1);
                    finalData.RemoveAt(finalData.Count - 1);
                }else
                {
                    finalData.Add(outData[outData.Count - 3]);
                    finalData.Add(outData[outData.Count - 2]);
                    finalData.Add(outData[outData.Count - 1]);
                }
            }


            return finalData;
        }
    }
}
