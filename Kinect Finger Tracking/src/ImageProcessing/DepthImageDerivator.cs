﻿using OpenCvSharp;
using KinectModule;

namespace ImageProcessing
{
    class DepthImageDerivator
    {
        private Mat outputFrame, gradX, gradY;
        private int scale = 1;
        private int delta = 0;
        private int ddepth = MatType.CV_16SC1;

        public DepthImageDerivator()
        {
            outputFrame = new Mat(KinectDepthImageGetter.H, KinectDepthImageGetter.W, MatType.CV_16UC1);
            gradX = new Mat(KinectDepthImageGetter.H, KinectDepthImageGetter.W, MatType.CV_16SC1);
            gradY = new Mat(KinectDepthImageGetter.H, KinectDepthImageGetter.W, MatType.CV_16SC1);
        }

        public Mat GetOutputFrame(Mat inputFrame)
        {
           
            Cv2.GaussianBlur(inputFrame, outputFrame, new Size(5, 5), 0, 0);

            Cv2.Sobel(outputFrame, gradX, ddepth, 1, 0, 5, scale, delta);

            Cv2.Sobel(outputFrame, gradY, ddepth, 0, 1, 5, scale, delta);
           
            Cv2.AddWeighted(gradX, 0.5, gradY, 0.5, 0, outputFrame);
           
            return outputFrame;
        }
    }
}
