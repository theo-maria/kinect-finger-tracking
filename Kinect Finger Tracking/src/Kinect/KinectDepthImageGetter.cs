﻿using System;
using OpenCvSharp;
using Microsoft.Kinect;

namespace KinectModule
{
    static public class KinectDepthImageGetter
    {
        static public Mat frame;
        static public UInt16[] frameData;
        static public DepthFrameReader reader;

        static public int H = 424, W = 512;

        static public void Initialize()
        {
            KinectSensor sensor = KinectSensor.GetDefault();
            DepthFrameSource source = sensor.DepthFrameSource;
            reader = source.OpenReader();

            reader.FrameArrived += DepthReaderFrameArrived;
            sensor.Open();

            //TODO : Initialiser taille de fenêtre de manière dynamique
            frameData = new UInt16[H * W];
            frame = new Mat(H, W, MatType.CV_16UC1);
        }

        private static void DepthReaderFrameArrived(object sender, DepthFrameArrivedEventArgs e)
        {
            using (DepthFrame fr = e.FrameReference.AcquireFrame())
            {
                if (fr != null)
                {
                    fr.CopyFrameDataToArray(frameData);
                    frame.SetArray(0, 0, frameData);
                }
            }
        }
    }
}