﻿using OpenCvSharp;
using Microsoft.Kinect;
using KinectModule;
using Displayer;
using ImageProcessing;
using System;
using System.Collections.Generic;

class Program
{
    private enum PlayingMode { LOAD_FROM_KINECT, SAVE_FROM_KINECT, LOAD_FROM_IMAGES};
    private static PlayingMode currentMode = PlayingMode.LOAD_FROM_KINECT;

    private static string SAVER_LOADER_USE_CASE = "FingerHover";

    private static DepthImageThresholder thresh;
    private static DepthImageDerivator derivator;
    private static FingerDetection fingerDetection;
    private static ClickDetection clickDetection;
    private static DepthImageDisplayer depthCameraDisplayer;
    private static DepthImageDisplayer thresholdDisplayer;
    private static DepthImageDisplayer derivativeDisplayer;
    private static DepthImageDisplayer detectionDisplayer;

    private static SaverLoader saverLoader;

    private static int imageCount = 0 ;
    private static Mat[] imagesSaver;
    private static Mat[] imagesLoaded;

    static void Main()
    {
        InitializeProcessingChain();
        switch(currentMode)
        {
            case PlayingMode.LOAD_FROM_KINECT:
                SetupLoadFromKinect();
                break;
            case PlayingMode.SAVE_FROM_KINECT:
                SetupSaveFromKinect();
                break;
            case PlayingMode.LOAD_FROM_IMAGES:
                SetupLoadFromImages();
                break;
        }

        WaitForQuit();
    }

    private static void InitializeProcessingChain()
    {
        depthCameraDisplayer = new DepthImageDisplayer("Depth Camera");
        thresholdDisplayer = new DepthImageDisplayer("Threshold");
        derivativeDisplayer = new DepthImageDisplayer("Derivative");
        detectionDisplayer = new DepthImageDisplayer("Detection");
        thresh = new DepthImageThresholder();
        derivator = new DepthImageDerivator();
        fingerDetection = new FingerDetection();
        clickDetection = new ClickDetection();
    }

    private static void ExecuteProcessingChain(Mat frame)
    {
        depthCameraDisplayer.Display(frame);
        frame = thresh.GetOutputFrame(frame);
        thresholdDisplayer.Display(frame);
        Mat frame2 = derivator.GetOutputFrame(frame);
        derivativeDisplayer.Display(frame2);
        List<int> fingersPosition = fingerDetection.FindCylinder(frame2, frame);
        Console.WriteLine(clickDetection.FindClick(frame, fingersPosition));
    }

    private static void SetupLoadFromKinect()
    {
        KinectDepthImageGetter.Initialize();
        KinectDepthImageGetter.reader.FrameArrived += DepthReaderFrameArrived;
    }

    private static void SetupSaveFromKinect()
    {
        KinectDepthImageGetter.Initialize();
        saverLoader = new SaverLoader();
        imagesSaver = new Mat[50];
        KinectDepthImageGetter.reader.FrameArrived += DepthReaderFrameArrivedSaver;
    }

    private static void SetupLoadFromImages()
    {
        saverLoader = new SaverLoader();
        imagesLoaded = saverLoader.Load(SAVER_LOADER_USE_CASE + "/" + SAVER_LOADER_USE_CASE);
    }

    private static void WaitForQuit()
    {
        char c = (char)0;
        while (c != 27)
        {
            if (currentMode == PlayingMode.LOAD_FROM_IMAGES)
                LoadNextImage();
            c = (char)Cv2.WaitKey(25);
        }
    }

    // Called on every frame caught by Kinect
    private static void DepthReaderFrameArrived(object sender, DepthFrameArrivedEventArgs e)
    {
        Mat frame = KinectDepthImageGetter.frame;
        ExecuteProcessingChain(frame);
    }


    private static void LoadNextImage()
    {
        Mat frame = imagesLoaded[imageCount % 50];
        ExecuteProcessingChain(frame);
        imageCount++;
    }

    private static void DepthReaderFrameArrivedSaver(object sender, DepthFrameArrivedEventArgs e)
    {
        Mat frame = KinectDepthImageGetter.frame;
        depthCameraDisplayer.Display(frame);
        frame = thresh.GetOutputFrame(frame);
        if (imageCount < 50)
        {
            imagesSaver[imageCount] = new Mat();
            frame.CopyTo(imagesSaver[imageCount]);
        }
        if (imageCount == 49)
        {
            Console.WriteLine("saved");
            saverLoader.Save(imagesSaver, SAVER_LOADER_USE_CASE + "/" + SAVER_LOADER_USE_CASE);
        }
        imageCount++;
        thresholdDisplayer.Display(frame);
    }
}