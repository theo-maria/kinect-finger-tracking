﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;
using KinectModule;

namespace KinectDisplayer
{
    class DepthImageDisplayer
    {
        private static double CONVERTION_FACTOR = 255.0 / 4096.0;
        private static string WINDOW_NAME = "Depth Camera";
        private static Mat frame_s;

        public static void Initialize()
        {
            frame_s = new Mat(KinectDepthImageGetter.H, KinectDepthImageGetter.W, MatType.CV_8UC1);
            Cv2.ImShow(WINDOW_NAME, frame_s);
        }

        public static void Display(Mat inputFrame)
        {
            if (inputFrame.Width > 0 && inputFrame.Height > 0)
            {
                inputFrame.ConvertTo(frame_s, MatType.CV_8UC1, CONVERTION_FACTOR);
                Cv2.ImShow(WINDOW_NAME, frame_s);
            }
        }
    }
}
