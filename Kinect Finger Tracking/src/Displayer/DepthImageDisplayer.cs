﻿using OpenCvSharp;
using KinectModule;

namespace Displayer
{
    class DepthImageDisplayer
    {
        private double CONVERTION_FACTOR = 255.0 / 4096.0;
        private string windowName;
        private Mat frame_s;

        public DepthImageDisplayer(string windowName)
        {
            frame_s = new Mat(KinectDepthImageGetter.H, KinectDepthImageGetter.W, MatType.CV_8UC1);
            this.windowName = windowName;
            Cv2.ImShow(windowName, frame_s);
        }

        public void Display(Mat inputFrame)
        {
            if (inputFrame.Width > 0 && inputFrame.Height > 0)
            {
                inputFrame.ConvertTo(frame_s, MatType.CV_8UC1, CONVERTION_FACTOR);
                Cv2.ImShow(windowName, frame_s);
            }
        }
    }
}
