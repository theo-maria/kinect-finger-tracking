﻿using OpenCvSharp;
using System.IO;
using System.Linq;
using System;
using KinectModule;

public class SaverLoader 
{
   public void Save(Mat[] frames, string name)
    {
        for (int i = 0; i < frames.Length; ++i) {
            Mat frame = frames[i];
            double[] arr = new double[frame.Width * frame.Height];
            for (int x = 0; x < frame.Width; ++x)
            {
                for (int y = 0; y < frame.Height; ++y)
                {
                    double val = frame.Get<ushort>(y, x);
                    arr[y * frame.Width + x] = val;
                }
            }
            File.WriteAllLines(name + i + ".dat", arr.Select(d => d.ToString()));
        }
    }

    public Mat[] Load(string name)
    {
        Mat[] imageLoader = new Mat[50];
        int height = KinectDepthImageGetter.H;
        int width = KinectDepthImageGetter.W;
        for(int i = 0; i < 50; ++i)
        {
            imageLoader[i] = new Mat(KinectDepthImageGetter.H, KinectDepthImageGetter.W, MatType.CV_16UC1);
            int currentWidth = 0, currentHeight = 0;
            string[] fileData = File.ReadAllLines(name + i + ".dat");
            foreach (string line in fileData)
            {
                ushort[] data = Array.ConvertAll(line.Split(','), ushort.Parse);
                imageLoader[i].Set(currentHeight, currentWidth, data[0]);
                ++currentWidth;
                if(currentWidth == width)
                {
                    currentWidth = 0;
                    currentHeight++;
                }
            }
        }
        return imageLoader;
    }

}